package org.expasy.sugarconverter.parser;

public class Constants {

	public static Character openingSquareBracket = '[';
	public static Character closingSquareBracket = ']';	
	public static Character openingParenthesis = '(';
	public static Character closingParenthesis = ')';
	public static Character openingCurlyBracket = '{';
	public static Character closingCurlyBracket = '}';
	public static Character doubleQuote = '"';
	public static String singleQuote = "'";
	
	public static char EOL = '\n';
	
	public static String DASH ="-";
	public static String PIPE ="|";
	
	
	public static String P_INTERNAL = "-P-";
	public static String P_TERMINAL = "-P";
	public static Character P = 'P';
	
	public static String UNKNOWN = "x";
	public static String UNKNOWN_LINKAGE_POS = "-1";
	
	public static String N_LINKED = "N-LINKED"; //(-> beta)
	public static String O_LINKED = "O-LINKED"; //(-> alpha)
	
	public static String LINKAGE_SEPARATOR = "+";
	public static String COLON = ":";
	
	public static String BASE_TYPE_MONOSAC = "b";
	public static String BASE_TYPE_SUBST = "s";
	public static String BASE_TYPE_REPEAT = "r";
	
	public static String UNKNOWN_IUPAC_LINKAGE = "(??-?)";
	public static String ROOT_RESIDUE_ID ="0.0";

}
