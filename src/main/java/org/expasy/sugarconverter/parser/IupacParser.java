package org.expasy.sugarconverter.parser;

import org.expasy.sugarconverter.exception.ParseResidueException;

public class IupacParser {
	
	private String ctSequence="";
	private String iupacSequence="";
	private String glycanType = null;
	private IupacTree tree = null;
	
	
	
	public IupacParser()
	{
		
	}
	
	public IupacParser(String sequence)
	{
		this.setIupacSequence(sequence.trim());
	}
	
	public IupacParser(String sequence,String glycanType)
	{
		this.setIupacSequence(sequence.trim());
		this.setGlycanType(glycanType);
	}
	
	public String getIupacSequence() {
		return iupacSequence;
	}


	public void setIupacSequence(String iupacSequence) {
		this.iupacSequence = iupacSequence.trim();
	}


	public String getCtSequence() {
		return ctSequence;
	}


	public void setCtSequence(String ctSequence) {
		this.ctSequence = ctSequence;
	}

	public IupacTree getTree() {
		return tree;
	}

	public String getGlycanType() {
		return glycanType;
	}

	public void setGlycanType(String glycanType) {
		this.glycanType = glycanType;
	}

	public IupacTree parse() throws Exception
	{
	
	tree = new IupacTree(this.iupacSequence);
	tree.setGlycanType(this.glycanType);
	try
	{
		tree.parse();
	}
	catch (Exception ex) 
	{
		
//		System.err.println("IupacParser parse() : "+ ex.getMessage());
		throw ex;
	}
//	tree.printTree(tree.getBranches());
	
//	setCtSequence(tree.convertToCT());
//	
//	//tests
//	tree.countIupacResidue();
//	tree.countCtResidue();
//	tree.printTree(tree.getCtSortedbranches());
	
	return tree;
	}
	
	public IupacTree getCtTree(IupacTree iupacTree) //throws Exception
	{
		String ct = null;
		try
		{
			ct = iupacTree.convertToCT().trim();
//			System.out.println("IupacParser getCtTree : "+ ct);
			
		}
		catch(ParseResidueException prex)
		{		
			ct=null;
			
			System.err.println("IupacParser getCtTree : "+prex.getMessage());
		}
		catch(Exception ex)
		{
			System.err.println("IupacParser getCtTree : "+ex.getMessage());
		}	
			setCtSequence(ct);
			//tests
			iupacTree.countIupacResidue();
			iupacTree.countCtResidue();
			//tree.getBranchesIds();
	//		tree.printTree(tree.getBranches());
			iupacTree.printTree(iupacTree.getCtSortedbranches());
		
	return iupacTree;
	}
	
}
