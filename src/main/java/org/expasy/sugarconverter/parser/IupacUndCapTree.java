package org.expasy.sugarconverter.parser;

import java.util.ArrayList;
import java.util.Collections;

public class IupacUndCapTree extends IupacTree {

	private ArrayList<IupacResidue> parents = new ArrayList<IupacResidue>();
	private IupacResidue residue = null;
	private IupacTree parentTree = null; 
//	private int firstResidueIdx = 0;
//	private int lastResidueIdx = 0;
	

	/*Constructor */
	public IupacUndCapTree(String input) {
		//super.originalSequence=input;
		super(input);
	}
	public IupacUndCapTree() {
	}
	

	/*Accessor */
	public IupacResidue getResidue() {
		return residue;
	}

	public void setResidue(IupacResidue residue) {
		this.residue = residue;
	}
	
//	public int getFirstResidueIdx() {
//		return firstResidueIdx;
//	}
//
//
//	public void setFirstResidueIdx(int firstResidueIdx) {
//		this.firstResidueIdx = firstResidueIdx;
//	}
//
//
//	public int getLastResidueIdx() {
//		return lastResidueIdx;
//	}
//
//
//	public void setLastResidueIdx(int lastResidueIdx) {
//		this.lastResidueIdx = lastResidueIdx;
//	}


	public IupacTree getParentTree() {
		return parentTree;
	}


	public void setParentTree(IupacTree parentTree) {
		this.parentTree = parentTree;
	}


	public void addParent(IupacResidue parent){
		this.parents.add(parent);
	}
	
	public void addParents(ArrayList<IupacResidue> parents){
		this.parents.addAll(parents);
	}

	public ArrayList<IupacResidue> getParents()
	{
		return this.parents;
	}
	
	public String getParentIds()
	{
		String ids ="";
//		System.err.println("IupacUndCapTree getParentIds  : " + parents.toString());					
		ArrayList<Integer> parentsIds = new ArrayList<Integer>();
		
		/*Get and sort the parent ids*/
		for(int i = 0 ; i <parents.size() ; i++)
		{
			parentsIds.add(parents.get(i).getAbstractResidue().getResNb());
		}
		Collections.sort(parentsIds);
		
		for(int j = 0 ; j <parentsIds.size() ; j++)
		{
			ids += parentsIds.get(j) + "|";
		}
//		System.err.println("IupacUndCapTree getParentIds  : " + ids);
		if(ids.length()!=0)
		{
			ids = ids.substring(0,ids.length()-1);
		}
		
		return ids;
	}
}
