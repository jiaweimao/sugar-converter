package org.expasy.sugarconverter.comparator;

import java.util.ArrayList;

import org.expasy.sugarconverter.parser.IupacBranch;
import org.expasy.sugarconverter.parser.IupacResidue;
import org.expasy.sugarconverter.residue.AbstractResidue;
import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.utils.TreeTools;

public class CtEdgeComparator extends AbstractComparator {
	
	/*Constructor*/
	public CtEdgeComparator(ArrayList<IupacBranch> branches) 
	{
		super(branches);
	}

	/*Methods*/
	public ArrayList<IupacBranch> compare()
	{
		ArrayList<IupacBranch> branches = getBranchesToCompare();

		
		
		return branches;
//		return this.getBranchesCompared();
	}
}
