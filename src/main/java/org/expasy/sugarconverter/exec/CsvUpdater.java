package org.expasy.sugarconverter.exec;

import org.expasy.sugarconverter.csv.CsvTranslator;

public class CsvUpdater {
	public static void main(String[] args) {
		System.out.println("CSV UPDATER");
		
		
		
		char delimiter = ';';
		char quoteChar = '\'';
		String csvExt = ".csv";
		String csvDirPath = "./src/main/java/org/expasy/sugarconverter/csv/";
		String iupacDirPath = csvDirPath;
		String glycoctDirPath = csvDirPath;
		String fileName = "bacterial_lectins";		
		String iupacFileName = fileName + "_iupac";
		String glycoctFileName = fileName + "_glycoct";
		String pyScriptName = "cfgToIupac.py";
		
		int cfgColIdx = 4;
		int iupacColIdx = cfgColIdx + 1;
		int glycanTypeColIdx = cfgColIdx + 2;
		
		CsvTranslator csvTranslator = new CsvTranslator(delimiter, quoteChar, csvExt
													, csvDirPath, iupacDirPath, glycoctDirPath
													, fileName, iupacFileName, csvExt, glycoctFileName, csvExt
													, pyScriptName
													, cfgColIdx, iupacColIdx, glycanTypeColIdx);
		
		csvTranslator.translate();		
	}
}
