package org.expasy.sugarconverter.residue.iupac.monosaccharide;

import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.Anomer;
import org.expasy.sugarconverter.sugar.Modifier;

public class HexOl extends Hex{

	public HexOl()
	{
		super.addToModifiers(new MonosaccharideModifier(Modifier.Aldi, 1));
        super.setAnomer(Anomer.OpenChain.getSymbol());
        super.setRingClosureStart("0");
        super.setRingClosureEnd("0");

	}
}
