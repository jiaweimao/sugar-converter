package org.expasy.sugarconverter.residue.iupac.monosaccharide;


import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.*;

public class Rha extends Man{

	public Rha ()
	{
		this.getMonosaccharide().setStereo(StereoConfig.L);
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 6));
	}
	
	
	
}
