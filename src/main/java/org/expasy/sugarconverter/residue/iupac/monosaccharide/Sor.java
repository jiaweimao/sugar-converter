package org.expasy.sugarconverter.residue.iupac.monosaccharide;

import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.*;

public class Sor extends Xyl{

	
	public Sor ()
	{
		//super.setStem(Stem.Xyl);	
		Monosaccharide m = getMonosaccharide();
		m.setStereo(StereoConfig.D);
		super.setMonosaccharide(m);
		super.addToModifiers(new MonosaccharideModifier(Modifier.Keto, 2));
	}
	

	
	
	
}
