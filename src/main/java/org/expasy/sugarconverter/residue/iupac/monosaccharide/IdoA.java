package org.expasy.sugarconverter.residue.iupac.monosaccharide;

import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.Modifier;

public class IdoA extends Ido{
	
	public IdoA ()
	{
		super.addToModifiers(new MonosaccharideModifier(Modifier.A, 6));

	}
}
