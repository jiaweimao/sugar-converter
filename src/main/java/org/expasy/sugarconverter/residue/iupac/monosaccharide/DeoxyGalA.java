package org.expasy.sugarconverter.residue.iupac.monosaccharide;

import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.Modifier;

public class DeoxyGalA extends GalA{

	public DeoxyGalA()
	{
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 0));

	}
}
