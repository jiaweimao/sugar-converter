package org.expasy.sugarconverter.residue.iupac.composed;


import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.Kdn;
import org.expasy.sugarconverter.residue.iupac.substituent.NAc;
import org.expasy.sugarconverter.sugar.Substituent;


public class NeuGc extends GenericComposedResidue{

	public NeuGc()
	{

		GenericMonosaccharideResidue neu = new Kdn();
		
		GenericSubstituentResidue gc = new GenericSubstituentResidue();
		gc.setSubstituent(Substituent.NGlycolyl);
		
		Link linkGc = new Link("5","1");
		gc.setLinkToPrevious(linkGc);
		
		super.setMonosaccharide(neu);
		super.addToSubstituents(gc);
	}
	
	
	
}
