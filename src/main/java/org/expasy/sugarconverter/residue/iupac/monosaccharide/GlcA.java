package org.expasy.sugarconverter.residue.iupac.monosaccharide;

import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.Modifier;

public class GlcA extends Glc{
	
	public GlcA ()
	{
		super.addToModifiers(new MonosaccharideModifier(Modifier.A, 6));

	}

}
