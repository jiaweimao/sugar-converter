package org.expasy.sugarconverter.residue.iupac.composed;


import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.Gal;
import org.expasy.sugarconverter.residue.iupac.substituent.NAc;


public class GalNAc extends GenericComposedResidue{

	public GalNAc()
	{
		GenericMonosaccharideResidue gal = new Gal();
		GenericSubstituentResidue nac = new NAc();
		
		Link linkAc = new Link("2","1");
		nac.setLinkToPrevious(linkAc);
		
		super.setMonosaccharide(gal);
		super.addToSubstituents(nac);
	}
	
	
	
}
