package org.expasy.sugarconverter.residue.iupac.substituent;

import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.sugar.Substituent;

public class Hso3 extends GenericSubstituentResidue{
//	private Substituent substituent=Substituent.Sulfate;
	public Hso3 ()
	{
		super.setSubstituent(Substituent.Sulfate);
	}
}
