package org.expasy.sugarconverter.residue.iupac.composed;


import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.Modifier;


public class DeoxyGlcN extends GlcN{

	public DeoxyGlcN()
	{
        super.getMonosaccharide().addToModifiers(new MonosaccharideModifier(Modifier.D, 0));
	}
	
	
	
}
