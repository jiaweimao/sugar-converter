package org.expasy.sugarconverter.residue.iupac.substituent;

import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.sugar.Substituent;

public class S extends GenericSubstituentResidue{
//	private Substituent substituent=Substituent.Sulfate;
	public S ()
	{
		super.setSubstituent(Substituent.Sulfate);
	}
}
