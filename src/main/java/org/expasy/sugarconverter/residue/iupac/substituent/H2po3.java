package org.expasy.sugarconverter.residue.iupac.substituent;

import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.sugar.Substituent;

public class H2po3 extends GenericSubstituentResidue{
//	private Substituent substituent=Substituent.Phosphate;
	
	public H2po3 ()
	{
		super.setSubstituent(Substituent.Phosphate);
	}
}
