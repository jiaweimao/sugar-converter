package org.expasy.sugarconverter.residue.iupac.substituent.virtual;

import org.expasy.sugarconverter.residue.GenericVirtualSubstituentResidue;
import org.expasy.sugarconverter.sugar.Substituent;

public class Cer extends GenericVirtualSubstituentResidue{
        
        
        public Cer ()
        {
                super.setSubstituent(Substituent.Ceramide);
        }
}

