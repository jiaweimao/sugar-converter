package org.expasy.sugarconverter.residue.iupac.composed;


import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.Gal;
import org.expasy.sugarconverter.residue.iupac.substituent.N;


public class GalN extends GenericComposedResidue{

	public GalN()
	{
		GenericMonosaccharideResidue gal = new Gal();
		GenericSubstituentResidue amino = new N();
		
		super.setMonosaccharide(gal);
		super.addToSubstituents(amino);
	}
	
	
	
}
