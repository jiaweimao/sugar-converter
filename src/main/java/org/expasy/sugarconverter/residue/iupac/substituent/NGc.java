package org.expasy.sugarconverter.residue.iupac.substituent;

import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.sugar.Substituent;

public class NGc extends GenericSubstituentResidue{
	

	
	public NGc ()
	{
		super.setSubstituent(Substituent.NGlycolyl);
	}
}
