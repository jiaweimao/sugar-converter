package org.expasy.sugarconverter.residue.iupac.monosaccharide;

import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.Modifier;

public class GalA  extends Gal{
	
	public GalA ()
	{
		super.addToModifiers(new MonosaccharideModifier(Modifier.A, 6));

	}
}
