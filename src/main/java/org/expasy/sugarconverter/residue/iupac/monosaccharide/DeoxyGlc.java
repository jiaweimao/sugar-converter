package org.expasy.sugarconverter.residue.iupac.monosaccharide;

import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.Modifier;

public class DeoxyGlc extends Glc{

	public DeoxyGlc()
	{
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 0));

	}
}
