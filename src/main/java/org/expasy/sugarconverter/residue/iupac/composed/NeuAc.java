package org.expasy.sugarconverter.residue.iupac.composed;


import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.Kdn;
import org.expasy.sugarconverter.residue.iupac.substituent.Ac;
import org.expasy.sugarconverter.residue.iupac.substituent.NAc;


public class NeuAc extends GenericComposedResidue{

	public NeuAc()
	{

		GenericMonosaccharideResidue neu = new Kdn();
		GenericSubstituentResidue ac = new NAc();
		
		Link linkAc = new Link("5","1");
		ac.setLinkToPrevious(linkAc);
		
		super.setMonosaccharide(neu);
		super.addToSubstituents(ac);
	}
	
	
	
}
