package org.expasy.sugarconverter.residue.iupac.monosaccharide;

import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.sugar.Stem;
import org.expasy.sugarconverter.sugar.StereoConfig;
import org.expasy.sugarconverter.sugar.Superclass;

public class Galf  extends Gal{

	public Galf ()
	{
		Monosaccharide m = getMonosaccharide();
//		m.setSuperClass(Superclass.Pen);
		
		super.setMonosaccharide(m);
		this.setRingClosureStart("1");
		this.setRingClosureEnd("4");
	}
}
