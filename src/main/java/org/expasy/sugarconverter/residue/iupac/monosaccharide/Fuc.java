package org.expasy.sugarconverter.residue.iupac.monosaccharide;



import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.*;

public class Fuc extends Gal{

	
	public Fuc ()
	{
		//super.addToMonosaccharides(new Monosaccharide(Stem.Gal));
		Monosaccharide m = getMonosaccharide();
		m.setStereo(StereoConfig.L);
		super.setMonosaccharide(m);
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 6));
	}
	
//	private Monosaccharide monsaccharide = new Monosaccharide();
//	
//	protected void init()
//	{
//		MonosaccharideResidue
//	}
	
	
	
}
