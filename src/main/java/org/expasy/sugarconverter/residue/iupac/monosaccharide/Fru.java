package org.expasy.sugarconverter.residue.iupac.monosaccharide;



import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.*;

public class Fru extends Ara{

	public Fru ()
	{
		
		super.addToModifiers(new MonosaccharideModifier(Modifier.Keto, 2));
	}
	
	
	
}
