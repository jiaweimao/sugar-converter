package org.expasy.sugarconverter.residue.iupac.monosaccharide;


import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.Modifier;
import org.expasy.sugarconverter.sugar.StereoConfig;

public class dAlt extends Alt{

	public dAlt ()
	{
		Monosaccharide m = getMonosaccharide();
		m.setStereo(StereoConfig.D);
		super.setMonosaccharide(m);
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 6));

	}
	
	
	
}
