package org.expasy.sugarconverter.residue.iupac.monosaccharide;

import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.Modifier;

public class GroA2 extends Gro{
	public GroA2 ()
	{
		super.addToModifiers(new MonosaccharideModifier(Modifier.A, 0));
		super.addToModifiers(new MonosaccharideModifier(Modifier.A, 0));
	}
}
