package org.expasy.sugarconverter.residue.iupac.substituent;

import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.sugar.Substituent;

public class N extends GenericSubstituentResidue{
	
	//private Substituent substituent=Substituent.Acetyl;
	
	public N ()
	{
		super.setSubstituent(Substituent.N);
	}
}
