package org.expasy.sugarconverter.residue.iupac.monosaccharide;

import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.sugar.*;

public class Gal extends GenericMonosaccharideResidue{

	
	public Gal ()
	{
		Monosaccharide m = new Monosaccharide(Stem.Gal);
		m.setStereo(StereoConfig.D);
		super.setMonosaccharide(m);		
	}
	
//	private Monosaccharide monsaccharide = new Monosaccharide();
//	
//	protected void init()
//	{
//		MonosaccharideResidue
//	}
	
	
	
}
