package org.expasy.sugarconverter.residue.iupac.monosaccharide;

import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.Modifier;

public class Qui extends Glc{

	public Qui ()
	{
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 6));
	}
}
