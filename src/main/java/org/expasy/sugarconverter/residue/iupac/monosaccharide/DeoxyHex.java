package org.expasy.sugarconverter.residue.iupac.monosaccharide;

import org.expasy.sugarconverter.parser.Constants;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.Modifier;

public class DeoxyHex extends Hex{
	public DeoxyHex()
	{
		//Monosaccharide m = getMonosaccharide();
		//m.setStereo(StereoConfig.L);
		//super.setMonosaccharide(m);
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 0));
	}
}
