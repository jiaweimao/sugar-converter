package org.expasy.sugarconverter.residue.iupac.monosaccharide;


import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.sugar.*;

public class Alt extends GenericMonosaccharideResidue{

	public Alt ()
	{
		Monosaccharide m = new Monosaccharide(Stem.Alt);
		super.setMonosaccharide(m);
		
	}
	
	
	
}
