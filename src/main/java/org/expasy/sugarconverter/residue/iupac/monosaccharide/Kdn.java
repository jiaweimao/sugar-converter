package org.expasy.sugarconverter.residue.iupac.monosaccharide;



import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.sugar.*;

public class Kdn extends GenericMonosaccharideResidue{

	
	public Kdn ()
	{
//		super.addToMonosaccharides(new Monosaccharide(Stem.Gro,"NON"));
//		super.addToMonosaccharides(new Monosaccharide(Stem.Gal,"NON"));
		Monosaccharide m = new Monosaccharide(Stem.Kdn);
		m.setStereo(StereoConfig.D);
		super.setMonosaccharide(m);
		
		super.addToModifiers(new MonosaccharideModifier(Modifier.A, 1));
		super.addToModifiers(new MonosaccharideModifier(Modifier.Keto, 2));
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 3));
	}
	
	
	
	
}
