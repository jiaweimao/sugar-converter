package org.expasy.sugarconverter.residue.iupac.composed;

import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.Man;
import org.expasy.sugarconverter.residue.iupac.substituent.NAc;

public class ManNAc  extends GenericComposedResidue{

	public ManNAc()
	{
		GenericMonosaccharideResidue glc = new Man();
		GenericSubstituentResidue nac = new NAc();
		
		Link linkAc = new Link("2","1");
		nac.setLinkToPrevious(linkAc);
		
		super.setMonosaccharide(glc);
		super.addToSubstituents(nac);
	}
	
}
