package org.expasy.sugarconverter.residue.iupac.composed;

import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.Kdn;
import org.expasy.sugarconverter.residue.iupac.substituent.NAc;

public class Neu5__Ac3 extends GenericComposedResidue{

	//TODO : set ac1,ac2 and ac3 position to (5,-1,-1)
	public Neu5__Ac3()
	{

		GenericMonosaccharideResidue neu = new Kdn();
		GenericSubstituentResidue ac1 = new NAc();
		GenericSubstituentResidue ac2 = new NAc();
		GenericSubstituentResidue ac3 = new NAc();
		
		Link linkAc1 = new Link("-1","1");
		Link linkAc2 = new Link("-1","1");
		Link linkAc3 = new Link("5","1");
		ac1.setLinkToPrevious(linkAc1);
		ac2.setLinkToPrevious(linkAc2);
		ac3.setLinkToPrevious(linkAc3);
		
		super.setMonosaccharide(neu);
		super.addToSubstituents(ac1);
		super.addToSubstituents(ac2);
		super.addToSubstituents(ac3);
	}
}