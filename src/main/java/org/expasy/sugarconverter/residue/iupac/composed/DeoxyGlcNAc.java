package org.expasy.sugarconverter.residue.iupac.composed;


import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.Modifier;


public class DeoxyGlcNAc extends GlcNAc{

	public DeoxyGlcNAc()
	{
		super.getMonosaccharide().addToModifiers(new MonosaccharideModifier(Modifier.D, 0));
	}
	
	
	
}
