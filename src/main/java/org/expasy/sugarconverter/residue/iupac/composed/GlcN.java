package org.expasy.sugarconverter.residue.iupac.composed;


import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.*;
import org.expasy.sugarconverter.residue.iupac.substituent.*;


public class GlcN extends GenericComposedResidue{

	public GlcN()
	{
		GenericMonosaccharideResidue glc = new Glc();
		GenericSubstituentResidue amino = new N();
		
		super.setMonosaccharide(glc);
		super.addToSubstituents(amino);
	}
	
	
	
}
