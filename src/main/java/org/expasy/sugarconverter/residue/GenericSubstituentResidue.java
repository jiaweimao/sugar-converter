package org.expasy.sugarconverter.residue;

import org.expasy.sugarconverter.parser.Constants;
import org.expasy.sugarconverter.sugar.*;

public class GenericSubstituentResidue extends AbstractResidue{

	//final Character baseType = 's';  
	
	private Substituent substituent;
	
	private int CtRESnumber;
	
	public int getCtRESnumber() {
		return CtRESnumber;
	}

	public void setCtRESnumber(int CtRESnumber) {
		this.CtRESnumber = CtRESnumber;
	}

	public Substituent getSubstituent() {
		return substituent;
	}

	public void setSubstituent(Substituent substituent) {
		this.substituent = substituent;
	}
	
	@Override
	public String toString() {
//		System.out.println("SubstituentResidue.toString() : " );
//		System.out.println("	substituent : " + this.substituent.getSymbol());
//		System.out.println("	abstractResidue number : " + this.getResNb());
//		
		
		return super.toString();
	}
	
	public String getCTvalue()
	{
		String result = "";
		
		result = this.getResNb() 	
					+ Constants.BASE_TYPE_SUBST //"s"//
					+ Constants.COLON 
					+ this.substituent.getSymbol();
					
		
//		System.out.println("GenericSubstituentResidue getCTvalue : " + result);			
					
		return result;
	}
}
