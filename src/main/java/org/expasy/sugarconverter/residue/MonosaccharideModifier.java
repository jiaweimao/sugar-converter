package org.expasy.sugarconverter.residue;


import org.expasy.sugarconverter.sugar.*;


public class MonosaccharideModifier{


	private Modifier modifier;
	private int position;
	
	public MonosaccharideModifier(Modifier modifier, int position)
	{
		this.modifier = modifier;
		this.position = position;
	}
	
	public Modifier getModifier() {
		return modifier;
	}

	public void setModifier(Modifier modifier) {
		this.modifier = modifier;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}
	
	@Override
	public String toString() {
		System.out.println("MonosaccharideModifier.toString() : " );
		System.out.println("	Modifier : " + this.modifier.getSymbol());
		System.out.println("	Position : " + this.getPosition());
		
		return super.toString();
	}
	
}
