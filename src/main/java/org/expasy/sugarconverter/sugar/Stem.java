package org.expasy.sugarconverter.sugar;
/**************************************************
*<p>  
*   
*   
*</p> 
* @author Julien Mariethoz
* 
*/
public enum Stem 
{
	//Stems with their "usual" superclass
	Gro("Glyceraldehyde","gro",Superclass.Tri),
	Ery("Erythrose","ery",Superclass.Tet),
	Rib("Ribose","rib",Superclass.Pen),
	Ara("Arabinose","ara",Superclass.Pen),
	All("Allose","all",Superclass.Hex),
	Alt("Altrose","alt",Superclass.Hex),
	Glc("Glucose","glc",Superclass.Hex),
	Man("Mannose","man",Superclass.Hex),
	Tre("Threose","tre",Superclass.Tet),
	Xyl("Xylose","xyl",Superclass.Pen),
	Lyx("Lyxose","lyx",Superclass.Pen),
	Gul("Gulose","gul",Superclass.Hex),
	Ido("Idose","ido",Superclass.Hex),
	Gal("Galactose","gal",Superclass.Hex),
	Tal("Talose","tal",Superclass.Hex),
	
	//TODO : compose from gro and gal and their respective isomer
	Kdn("Kdn","gro-dgal",Superclass.Non),
		
	//Virtual
	Hex("Hexose","",Superclass.Hex),
	Pent("Pentose","",Superclass.Pen)
	;

    /** Stem name */
    private String fullname;
	/** Stem short name. */
    private String symbol;
    private Superclass superclass;
    
    
    


	/** Private constructor, see the forName methods for external use. */
    private Stem( String fullname, String symbol, Superclass superclass ) 
    { 
        this.fullname = fullname; 
        this.symbol = symbol; 
        this.superclass = superclass;
    }
     
    
    /** Returns the appropriate Stem instance for the given symbol.  */
    public static Stem fromString(String symbol) {
        if (symbol != null) 
        {
          for (Stem s : Stem.values()) 
          {
            if (symbol.equalsIgnoreCase(s.symbol)) 
            {
              return s;
            }
          }
        }
        return null;
      }
    
    public String getFullname() {
		return fullname;
	}

	public String getSymbol() {
		return symbol;
	}

	public Superclass getSuperclass() {
		return superclass;
	}

//	public void setSuperclass(String superclass) {
//		this.superclass = superclass;
//	}
    /*
    public static Stem forName( String stem ) 
    { 
        switch (stem)
        {
        case "gro": return Gro;
        case "ery": return Ery;
        case "rib": return Rib;
        case "ara": return Ara;
        case "all": return All;
        case "alt": return Alt;
        case "glc": return Glc;
        case "man": return Man;
        case "tre": return Tre;
        case "xyl": return Xyl;
        case "lyx": return Lyx;
        case "gul": return Gul;
        case "ido": return Ido;
        case "gal": return Gal;
        case "tal": return Tal;
        }
    	*/
    }