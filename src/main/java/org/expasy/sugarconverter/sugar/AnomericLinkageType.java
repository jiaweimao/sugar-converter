package org.expasy.sugarconverter.sugar;

	
public enum AnomericLinkageType
{
    /** @see Anomer#Alpha */
    Glycosidic_Alpha,
    
    /** @see Anomer#Beta */
    Glycosidic_Beta,
    
    /** @see Anomer#Unknown */
    Glycosidic,
    
    /** Linkage type unknown or unspecified */
    Unknown,
    
    /** Linkage type unlisted */
    Other,
    
    /** Linkage type not applicable */
    None
    ;
}


