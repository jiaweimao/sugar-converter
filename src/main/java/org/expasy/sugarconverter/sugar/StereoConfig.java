package org.expasy.sugarconverter.sugar;


public enum StereoConfig //implements PotentiallyIndefinite
{
    D( 'd', "dextro" ),
    L( 'l', "levo" ),
    M( 'm', "meso" ),
    X( 'x', "unknown" ),
    UnknownStereoConfig( '?', "unknown" )
    ;
    
    /** single char representation of this stereo-config */
    private final Character symbol;
    private final String name;
    
    public Character getSymbol() {
		return symbol;
	}

	public String getName() {
		return name;
	}
    
    
    private StereoConfig( Character c, String s )
    {
        symbol = c;    
        name = s;
    }
    
    /*
    public static final StereoConfig forName( Character letter )
    throws IllegalArgumentException
    {
        char c = Character.toUpperCase( letter );
        switch ( c )
        {
            case 'D':   
                return D;
                
            case 'L':
                return L;                          
                
            case '?':
            case 'X':
            case 'U':
                return UnknownStereoConfig;
                
            case 'M':
                return M;
                
            default:
                throw new IllegalArgumentException(
                    "UnknownStereoConfig or unrecognised stereo configuration: "
                    + "valid values are: " + StereoConfig.values() 
                );
        }
    }
	*/
    /*
    public static final StereoConfig forName( String name )
    {
        if ( name == null || name.length() == 0 )
            throw new IllegalArgumentException(
                "Null or zero-length argument" );
        
        return forName( name.charAt( 0 ) );
    }
    */
    
    /** 
    *   Returns a {@link StereoConfig} that results from stereochemical
    *   inversion of the given StereoConfig; ie: {@link #D} becomes {@link #L},
    *   L becomes D, meso stays as meso, unknown stays as unknown.
    */
    /*
    public static final StereoConfig invert( StereoConfig sc )
    throws NullPointerException
    {
        switch ( sc )
        {
            case D:
                return L;
                
            case L:
                return D;
                
            default:
                return sc;
        }
    }
    */
    
    /** Returns the appropriate Stem instance for the given symbol.  */
    public static StereoConfig fromString(String symbol) {
        if (symbol != null) 
        {
          for (StereoConfig s : StereoConfig.values()) 
          {
            if (symbol.equalsIgnoreCase(s.symbol.toString())) 
            {
              return s;
            }
          }
        }
        return null;
      }
    
    public final boolean isDefinite()
    {
        return this != UnknownStereoConfig;   
    }
    
    
    public Character toChar()
    {
        return symbol;
    }

} // end enum



