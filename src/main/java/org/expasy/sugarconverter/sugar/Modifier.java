package org.expasy.sugarconverter.sugar;

public enum Modifier {

	D("deoxygenation","d"),
	S("acidic function","s"),
	Keto("carbonyl function","keto"),
	A("acidic function","a"),
	En("double bond","en"),
	Enx("double bond unkown position","enx"),
	Aldi("reduction of C1-carbonyl","aldi"),
	Sp2("outgoing linkage (double bond)","sp2"),
	Sp("outgoing linkage (triple bond)","sp"),
	Geminal("2 identical substitution","geminal");

	
	private String description;
    private String symbol;
//    private int position;
    
    
    


	/** Private constructor, see the forName methods for external use. */
    private Modifier( String description, String symbol ) 
    { 
        this.description = description; 
        this.symbol = symbol; 
    }
    
    
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}   
	
//	public int getPosition() {
//		return position;
//	}
//
//
//	public void setPosition(int position) {
//		this.position = position;
//	}
    
	/*	
    public static Modifier forName( String modifier ) 
    { 
    	
        switch ( Modifier.toModifier(modifier) )
        {
        case "d": return D;
        case "s": return S;
        case "keto" return Keto;
        case "en" return En;
        case "enx" return Enx;
        case "aldi" return Aldi;
        case "sp2" return Sp2;
        case "sp" return Sp;
        case "geminal" return Geminal;
        }
    }
    */
    public static Modifier fromString(String symbol) {
        if (symbol != null) 
        {
          for (Modifier m : Modifier.values()) 
          {
            if (symbol.equalsIgnoreCase(m.symbol)) 
            {
              return m;
            }
          }
        }
        return null;
      }
}
