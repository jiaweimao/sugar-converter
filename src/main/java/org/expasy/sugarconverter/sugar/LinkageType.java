package org.expasy.sugarconverter.sugar;


public enum LinkageType
{
	o('o', "hydrogen from OH – function removed and substituent attached at this position" ),
	h('h', "hydrogen removed and substituent attached at this position" ),
	d('d', "OH-function removed and substituted" ),
	n('n', "linkage to substituents, non-monosaccharide entities, repeat or statistical units" ),
	UnknownLinkageType('x', "unknown linkage type" ),
	r('r', "prochiral H-atom removed, resulting in R-configuration" ),
	s('s', "prochiral H-atom removed, resulting in S-configuration" )

    ;
    
    /** single char representation of this linkage type */
    private final Character symbol;
    private final String description;
    
    public Character getSymbol() {
		return symbol;
	}

	public String getDescription() {
		return description;
	}
    
    
    private LinkageType( Character c, String s )
    {
        symbol = c;    
        description = s;
    }
    
    
    /** Returns the appropriate Stem instance for the given symbol.  */
    public static LinkageType fromString(String symbol) {
        if (symbol != null) 
        {
          for (LinkageType s : LinkageType.values()) 
          {
            if (symbol.equalsIgnoreCase(s.symbol.toString())) 
            {
              return s;
            }
          }
        }
        return null;
      }
    
    public final boolean isDefinite()
    {
        return this != UnknownLinkageType;   
    }
    
    
    public Character toChar()
    {
        return symbol;
    }

} // end enum

