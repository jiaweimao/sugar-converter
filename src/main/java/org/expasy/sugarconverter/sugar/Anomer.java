package org.expasy.sugarconverter.sugar;


public enum Anomer
{
    /** Indicates the alpha anomeric configuration */
    Alpha("alpha", "a", AnomericLinkageType.Glycosidic_Alpha ),
    
    /** Indicates the beta anomeric configuration */
    Beta("beta", "b", AnomericLinkageType.Glycosidic_Beta ),
    
    /** Indicates that there is no anomeric configuration in effect, because the
    *   relevant monosaccharide is in open-chain form. */
    OpenChain("open-chain", "o", AnomericLinkageType.None ),
    
    /** Indicates the anomeric configuration is not known. */
    UnknownAnomer("unknown", "x", AnomericLinkageType.Unknown ),
    
    /** Indicates that an anomeric configuration is not applicable. */
    None("(no anomer)", " ", AnomericLinkageType.None )
    ;
    
    /** The default anomer; currently "unknown". */
    public static Anomer DefaultAnomer = None;
    
    /** Anomer verbose name */
    private String fullname;
    
    /** Anomer short name. */
    private String symbol;
    
    private AnomericLinkageType type;
    
    
    /** Private constructor, see the forName methods for external use. */
    private Anomer( String fullname, String symbol, AnomericLinkageType type ) 
    { 
        this.fullname = fullname; 
        this.symbol = symbol; 
        this.type = type;
    }
    
    
    /** Returns the appropriate Anomer instance for the given String.  */
    /*
    public  Anomer forName( String anomer )
    {  
        return forName( anomer.charAt(0) );  
    }
    */
    
    /** Returns the appropriate Anomer instance for the given character/symbol.  */
    /*
    public static Anomer forName( char anomer ) 
    { 
        switch ( anomer )
        {
            case 'a':
            case 'A': 
                return Alpha;
            
            case 'b': 
            case 'B': 
                return Beta;
            
            case '?': 
            case 'u':
            case 'x':
                return UnknownAnomer;
            
            case 'o': 
            case 'O': 
                return OpenChain;
            
            default: 
                return None;
        }
    }
    */
    
    public static  Anomer fromString(String symbol) {
        if (symbol != null) 
        {
          for (Anomer a : Anomer.values()) 
          {
            if (symbol.equalsIgnoreCase((a.symbol))) 
            {
              return a;
            }
            if (symbol.equals("?")||symbol.equals("x")||symbol.equals("X")) 
            {
              return Anomer.UnknownAnomer;
            }
          }
        }
        return null;
      }
    
    
    /** Returns this anomer's full name - "alpha", "beta", etc  */
    public String getFullname() {  return fullname;  }
    
    
    /** Returns the abbreviated name (symbol) of this anomer - "a", "b".  */
    public String getName() {  return ( this.equals(None) ) ? "" : "" + getSymbol();  }

    
    /** Returns the abbreviated name (symbol) of this anomer as a char - 'a', 'b'.  */
    public String getSymbol() {  return (this.equals(None)) ? "" : "" + symbol;  }
    
    
    public AnomericLinkageType getType() {  return type;  }
    
    
    public boolean isDefinite() {  return ! ( this == UnknownAnomer || this == None );  }
    
    
    /** Returns the abbreviated name (symbol) of this anomer, same as {@link getSymbol()}.  */
    //public char toChar() {  return symbol;  }
    
    
    /** Returns the short name (symbol) representing this anomer.  */
    //public String toString() {  return getSymbol();  }
    
} // end enum Anomer



