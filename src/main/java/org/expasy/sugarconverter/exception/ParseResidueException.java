package org.expasy.sugarconverter.exception;

public class ParseResidueException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6208071229717657334L;

	public ParseResidueException()
	{
		
	}
	public ParseResidueException(String message)
	{
		super(message);
	}
}
