package org.expasy.sugarconverter.database;

import org.expasy.sugarconverter.parser.Constants;

public class DbNames {
	public static String glycosuite = "GlycoSuiteDB";
	public static String sugarbind = "SugarBindDB";
	public static String unicarb = "UniCarbDB";
	public static String gssb = "GSSB";
	public static String sugarbind12 = "SugarBindDB12";
	public static String gssb12 = "GSSB12";
	
	public static String user = "julien";
	public static String password = "000000";
	public static boolean ssl = true;
    public static String sslfactory = "org.postgresql.ssl.NonValidatingFactory";

	
	
	
	public static String getQuerySelect(String dbName)
	{
	String querySelectGS = "SELECT "+Constants.doubleQuote+"STRUCTURE_ID"+Constants.doubleQuote+", "+Constants.doubleQuote+"GLYCAN_ST"+Constants.doubleQuote+", "+Constants.doubleQuote+"GLYCAN_TYPE"+Constants.doubleQuote+" from glycodb."+Constants.doubleQuote+"STRUCTURE"+Constants.doubleQuote+" ";
	/*
	SELECT * from glycodb."STRUCTURE"
	ALTER TABLE glycodb."STRUCTURE" ADD COLUMN "GLYCAN_ST_CT" character varying(1500);
	ALTER TABLE glycodb."STRUCTURE" ADD COLUMN "GLYCAN_ST_CT_MOD_TIME" character varying(150);
	*/
	String querySelectSB = "SELECT ligand_id, std_nomenclature, null FROM nomenclature ";
	/*
	SELECT * from nomenclature
	ALTER TABLE nomenclature ADD COLUMN "GLYCAN_ST_CT" character varying(1500);
	ALTER TABLE nomenclature ADD COLUMN "GLYCAN_ST_CT_MOD_TIME" character varying(150);
	*/
	String querySelectUni = "";
	String querySelectGsSb = querySelectGS + " union all " + querySelectSB;
	String querySelectSB12 = querySelectSB;
	String querySelectGsSb12 = querySelectGsSb;
	
	if(dbName.equals(glycosuite))
	{return querySelectGS;
	}
	if(dbName.equals(sugarbind))
	{return querySelectSB;
	}
	if(dbName.equals(unicarb))
	{return querySelectUni;
	}
	if(dbName.equals(gssb))
	{return querySelectGsSb;
	}
	if(dbName.equals(sugarbind12))
	{return querySelectSB12;
	}
	if(dbName.equals(gssb12))
	{return querySelectGsSb12;
	}
	return null;
	}
}
